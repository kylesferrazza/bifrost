terraform {
  backend "remote" {
    organization = "kylesferrazza"

    workspaces {
      name = "bifrost"
    }
  }
}

variable "gcp_key" {
  type = string
}

variable "username" {
  type = string
  default = "kyle"
}

variable "email" {
  type = string
  default = "kyle.sferrazza@gmail.com"
}

variable "domain" {
  type = string
  default = "kylesferrazza.com"
}

variable "ssh_pubkey" {
  type = string
}

provider "google" {
  credentials = var.gcp_key
  project = "bifrost-ks"
  region  = "us-east1"
  zone    = "us-east1-b"
}

resource "google_compute_address" "bifrost_ip" {
  name = "bifrost-ip"
}

resource "google_compute_firewall" "default" {
  name = "web-firewall"
  network = "default"

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports = ["80", "443"]
  }

  source_ranges = ["0.0.0.0/0"]
  target_tags = ["web"]
}

resource "google_compute_instance" "bifrost_vm" {
  name = "bifrost"
  machine_type = "f1-micro"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-9"
    }
  }

  tags = ["web"]

  network_interface {
    network = "default"
    access_config {
      nat_ip = google_compute_address.bifrost_ip.address
      #public_ptr_domain_name = "bifrost.${var.domain}"
    }
  }

  metadata = {
    ssh-keys = "${var.username}:${var.ssh_pubkey}"
  }
}

output "privkey" {
  value = acme_certificate.ssl_cert.private_key_pem
  sensitive = true
}

output "fullchain" {
  value = "${acme_certificate.ssl_cert.certificate_pem}\n${acme_certificate.ssl_cert.issuer_pem}"
  sensitive = true
}

output "bifrost-ip" {
  value = google_compute_address.bifrost_ip.address
}

provider "acme" {
  server_url = "https://acme-v02.api.letsencrypt.org/directory"
}

resource "tls_private_key" "reg_private_key" {
  algorithm = "RSA"
}

resource "acme_registration" "reg" {
  account_key_pem = tls_private_key.reg_private_key.private_key_pem
  email_address = var.email
}

variable "cloudflare_api_token" {
  type = string
}

resource "acme_certificate" "ssl_cert" {
  account_key_pem = acme_registration.reg.account_key_pem
  common_name = "bifrost.${var.domain}"
  subject_alternative_names = ["*.bifrost.${var.domain}"]

  dns_challenge {
    provider = "cloudflare"
    config = {
      CF_DNS_API_TOKEN = var.cloudflare_api_token
    }
  }
}

variable "cloudflare_zone_id" {
  type = string
}

provider "cloudflare" {
  api_token = var.cloudflare_api_token
}

resource "cloudflare_record" "bifrost_record" {
  zone_id = var.cloudflare_zone_id
  name = "bifrost"
  value = google_compute_address.bifrost_ip.address
  type = "A"
  ttl = 300
}

resource "cloudflare_record" "bifrost_wildcard_record" {
  zone_id = var.cloudflare_zone_id
  name = "*.bifrost"
  value = google_compute_address.bifrost_ip.address
  type = "A"
  ttl = 300
}
